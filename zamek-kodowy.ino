#include <LiquidCrystal_I2C.h>
#include <Bounce2.h>

Bounce maskBtn = Bounce();

LiquidCrystal_I2C lcd(0x3F, 20, 4);

#define PIN_LEN 8
#define BTN_PIN 13
#define LED_R_PIN 6
#define LED_G_PIN 7
#define BUZZER_PIN 8
#define SCREEN_OFF_TIMEOUT 10000

char enteredPin[PIN_LEN + 1] = {};
char setPin[PIN_LEN + 1] = "7355608";
uint8_t enteredCount = 0;
int reading;
bool masking = false;
uint16_t lastNumber;
long lastInterationTime = 0;

void initScreen();
void clearPin();
uint16_t readNumber();
uint8_t countBits(uint16_t num);
int8_t oldestBitPosition(uint16_t num);
void wrongPin();
void goodPin();
void clearSecondLine();
void printEnteredPin();
void changeMaskingIcon(bool masking);
void changePin();
void message(String message);

#if PIN_LEN > 14
#error "PIN LEN CANNOT BE GREATER THAN 14"
#endif

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 2; i < 6; ++i) {
    pinMode(i, OUTPUT);
  }
  for (int i = 9; i < 13; ++i) {
    pinMode(i, INPUT_PULLUP);
  }
  pinMode(LED_G_PIN, OUTPUT);
  pinMode(LED_R_PIN, OUTPUT);

  maskBtn.attach(BTN_PIN, INPUT_PULLUP);

  maskBtn.interval(100);
  initScreen();
}

void initScreen() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Podaj haslo...");
  lcd.setCursor(0, 1);
  lcd.blink();
  changeMaskingIcon(masking);
}

void clearPin() {
  for (int i = 0; i < enteredCount; ++i) {
    enteredPin[i] = 0;
  }
  enteredCount = 0;
}

void message(String message) {
  lcd.clear();
  lcd.print(message);
  delay(3000);
  lcd.clear();
}

void changePin() {
  const bool mask_before = masking;
  changeMaskingIcon(false);
  masking = false;
  lcd.clear();
  lcd.noBlink();
  lcd.print("Zmiana PINu");
  lcd.setCursor(0, 1);
  lcd.print("* - Anuluj/OK");

  noTone(BUZZER_PIN);
  for (int i = 0; i < 5; ++i) {
    tone(BUZZER_PIN, 4000, 80);
    delay(200);
  }

  long startTime = millis();
  while (millis() - startTime < 3000) {
    maskBtn.update();
    if (maskBtn.rose()) {
      tone(BUZZER_PIN, 600, 300);
      message("Anulowanie");
      clearPin();
      changeMaskingIcon(mask_before);
      masking = mask_before;
      initScreen();
      return;
    }
  }

  lcd.clear();
  lcd.blink();
  lcd.print("Podaj nowy PIN...");
  lcd.setCursor(0, 1);
  uint8_t bitCount;
  uint16_t state;
  uint16_t lastState;
  clearPin();

  do {
    do {
      lastState = state;
      state = readNumber();
      bitCount = countBits(state);
      maskBtn.update();
      if (maskBtn.fell()) {
        if (enteredCount < 1) {
          tone(BUZZER_PIN, 600, 300);
          message("Anulowanie");
          clearPin();
          initScreen();
          changeMaskingIcon(mask_before);
          masking = mask_before;
          return;
        }
        goto saveNewPin;
      }
    } while (bitCount == 0 || lastState == state);

    int8_t oldestBit = oldestBitPosition(state);

    char hexString[2];
    itoa(oldestBit, hexString, 16);
    enteredPin[enteredCount++] = hexString[0];

    printEnteredPin();
  } while (enteredCount < PIN_LEN);

  lcd.clear();

saveNewPin:
  for (int i = 0; i < PIN_LEN; ++i) {
    if (i < enteredCount) {
      setPin[i] = enteredPin[i];
    } else {
      setPin[i] = 0;
    }
  }
  clearPin();
  changeMaskingIcon(mask_before);
  masking = mask_before;
  tone(BUZZER_PIN, 1200, 200);
  message("Zmieniono PIN!");
}

uint16_t readNumber() {
  uint16_t num = 0;
  for (int i = 2; i < 6; ++i) {
    digitalWrite(i, LOW);
    for (int j = 9; j < 13; ++j) {
      reading = digitalRead(j);
      num <<= 1;
      num += !reading;
    }
    for (int i = 2; i < 6; ++i) {
      digitalWrite(i, HIGH);
    }
  }
  return num;
}

uint8_t countBits(uint16_t num) {
  // Counting bits set, Brian Kernighan's way
  // from http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
  // count the number of bits set in num
  uint8_t c;  // c accumulates the total bits set in v
  for (c = 0; num; c++) {
    num &= num - 1;  // clear the least significant bit set
  }

  return c;
}

int8_t oldestBitPosition(uint16_t num) {
  for (int8_t pos = 15; pos >= 0; --pos) {
    if ((num >> pos) & 1) {
      return pos;
    }
  }
  return 0;
}


void wrongPin() {
  digitalWrite(LED_R_PIN, HIGH);
  lcd.noCursor();
  lcd.noBlink();
  lcd.clear();
  lcd.home();
  lcd.print("Zle haslo!");
  for (int i = 0; i < 3; i++) {
    tone(BUZZER_PIN, 1000, 100);
    delay(300);
  }

  digitalWrite(LED_R_PIN, LOW);
  clearPin();
  initScreen();
}

void goodPin() {
  maskBtn.update();
  digitalWrite(LED_G_PIN, HIGH);
  lcd.noCursor();
  lcd.noBlink();
  lcd.clear();
  lcd.home();
  lcd.print("Otwieranie!");
  lcd.setCursor(0, 1);
  lcd.print("* - zmien kod");
  tone(BUZZER_PIN, 2000, 1200);
  long startTime = millis();
  while (millis() - startTime < 3000) {
    maskBtn.update();
    if (maskBtn.rose()) {
      changePin();
      clearPin();
      initScreen();
      digitalWrite(LED_G_PIN, LOW);
      return;
    }
  }

  digitalWrite(LED_G_PIN, LOW);
  message("Zamykanie...");
  clearPin();
  initScreen();
}

void clearSecondLine() {
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(0, 1);
}

void printEnteredPin() {
  clearSecondLine();
  if (masking) {
    for (int i = 0; i < enteredCount; ++i) {
      lcd.print('*');
    }
  } else {
    lcd.print(enteredPin);
  }
}

void changeMaskingIcon(bool masking) {
  lcd.setCursor(15, 0);
  lcd.print(masking ? '*' : 'A');
  lcd.setCursor(enteredCount, 1);
}

void loop() {
  uint16_t number = 0;
  lastNumber = 0;

  do {
    maskBtn.update();
    if (maskBtn.fell()) {
      masking = !masking;
      changeMaskingIcon(masking);
      printEnteredPin();

      lastInterationTime = millis();
      lcd.backlight();
    }
    if (millis() - lastInterationTime > SCREEN_OFF_TIMEOUT) {
      clearPin();
      printEnteredPin();
      lcd.noBacklight();
    }
    lastNumber = number;
    number = readNumber();
  } while (number != 0 || lastNumber == number);

  lastInterationTime = millis();
  lcd.backlight();

  delay(20);
  uint8_t numBits = countBits(lastNumber);
  int8_t oldestBit = oldestBitPosition(lastNumber);

  char hexString[2];
  itoa(oldestBit, hexString, 16);
  enteredPin[enteredCount++] = hexString[0];

  printEnteredPin();

  bool pinGood = true;
  for (int pos = 0; pos < PIN_LEN; ++pos) {
    if (enteredPin[pos] != setPin[pos])
      pinGood = false;
  }

  if (pinGood) {
    goodPin();
    lastInterationTime = millis();
  }

  if (enteredCount == PIN_LEN) {
    wrongPin();
    return;
  }
}
