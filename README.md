### 1. Temat, tytuł realizowanego projektu

**Projekt - zamek na kod**

### 2. Cel, motywacja, analiza wartości technicznych i dydaktycznych w zakresie opracowania

Celem projektu było zaprogramowanie mikrokontrolera i stworzenie układu umożliwiającego sterowanie na przykład zamkiem magnetyczym, 

### 3. Opis techniczny uzasadniający zastosowane rozwiązania i szczegółowo wyjaśniający zastosowane elementy projektu

Zastosowane zostały:

- **Moduł główny Maker Uno** (kompatybilny z Arduino Uno) – wykorzystany do sterowania całym układem, oparty jest na układzie ATmega328P. Ponadto wbudowany w tę płytkę buzzer pozwolił na zmniejszenie ilości wymaganych elementów zewnętrznych. Ilość pinów wejścia/wyjścia w tym układzie również odpowiadała zapotrzebowaniu tego projektu.
- **Wyświetlacz LCD** 2x16 znaków z konwerterem I2C – pozwolił na wyświetlanie komunikatów do użytkownika. Została wybrana ta wersja ze względu na niski pobór prądu i łatwość obsługi. Konwerter I2C umożliwił komunikację z wyświetlaczem za pomocą jedynie dwóch przewodów.
- **Macierz przycisków** 4x4 – została wykorzystana jako wejście użytkownika (klawiatura numeryczna w systemie szesnastkowym)
- **Przycisk tact switch** – dodatkowy przycisk pozwalający na wyzwalanie większej ilości akcji
- 3x **LED** – użyte do komunikacji statusu dla użytkownika i wizualnej informacji zwrotnej. Służą też jako zastępstwo dla przekaźnika symbolizując otwarcie zamka.

### 4. Kosztorys uwzględniający dane techniczne, ilość i rodzaj wszystkich komponentów, które zostaną wykorzystane w projekcie

| Nazwa                                                        |                cena |
| ------------------------------------------------------------ | ------------------: |
| Modół główny Arduino (Arduino Uno lub inny kompatybilny tu: Cytron Maker Uno) |              50 PLN |
| Wyświetlacz LCD 2x16 znaków + konwerter I2C                  |              25 PLN |
| Buzzer pasywny lub aktywny (jeśli nie ma wbudowanego na płytce) |               3 PLN |
| Przewody połączeniowe                                        | (zestaw) ok. 10 PLN |
| Płytka stykowa                                               |               7 PLN |
| Klawiatura - matryca 16 x tact switch (lub 16 sztuk tact switch) |               7 PLN |
| Tact switch (przycisk '*')                                   |             < 1 PLN |
| LED x3 (żółta, zielona, czerwona) + rezystory                |     (zestaw) 10 PLN |
| (opcjonalnie) przekaźnik z optoizolacją, cewka 5V            |               8 PLN |
| **SUMA:**                                                    |         **121 PLN** |

*Uwaga: ceny orientacyjne, stan ze stycznia 2024*

### 5. Obliczenia parametrów poszczególnych konfiguracji rozwiązań

Użyte dla LED rezystory mają wartość $330 \Omega$.

### 6. Rysunki techniczne obejmujące schematy połączeń, schematy ideowe w przypadku zastosowania elementów nietypowych, algorytm aplikacji

![image-20240109204210789](./assets/image-20240109204210789.png)

Kod dla mikrokontrolera dostępny jest pod adresem https://gitlab.com/-/snippets/3637562 oraz w listingu poniżej

```c
#include <LiquidCrystal_I2C.h>
#include <Bounce2.h>

Bounce maskBtn = Bounce();
LiquidCrystal_I2C lcd(0x3F, 20, 4);

#define PIN_LEN 8
#define BTN_PIN 13
#define LED_R_PIN 6
#define LED_G_PIN 7
#define BUZZER_PIN 8
#define SCREEN_OFF_TIMEOUT 10000

char enteredPin[PIN_LEN + 1] = {};
char setPin[PIN_LEN + 1] = "7355608";
uint8_t enteredCount = 0;
int reading;
bool masking = false;
uint16_t lastNumber;
long lastInterationTime = 0;

void initScreen();
void clearPin();
uint16_t readNumber();
uint8_t countBits(uint16_t num);
int8_t oldestBitPosition(uint16_t num);
void wrongPin();
void goodPin();
void clearSecondLine();
void printEnteredPin();
void changeMaskingIcon(bool masking);
void changePin();
void message(String message);

#if PIN_LEN > 14
#error "PIN LEN CANNOT BE GREATER THAN 14"
#endif

void setup() {
  lcd.init();
  lcd.backlight();
  for (int i = 2; i < 6; ++i) {
    pinMode(i, OUTPUT);
  }
  for (int i = 9; i < 13; ++i) {
    pinMode(i, INPUT_PULLUP);
  }
  pinMode(LED_G_PIN, OUTPUT);
  pinMode(LED_R_PIN, OUTPUT);

  maskBtn.attach(BTN_PIN, INPUT_PULLUP);

  maskBtn.interval(100);
  initScreen();
}

void initScreen() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Podaj haslo...");
  lcd.setCursor(0, 1);
  lcd.blink();
  changeMaskingIcon(masking);
}

void clearPin() {
  for (int i = 0; i < enteredCount; ++i) {
    enteredPin[i] = 0;
  }
  enteredCount = 0;
}

void message(String message) {
  lcd.clear();
  lcd.print(message);
  delay(3000);
  lcd.clear();
}

void changePin() {
  const bool mask_before = masking;
  changeMaskingIcon(false);
  masking = false;
  lcd.clear();
  lcd.noBlink();
  lcd.print("Zmiana PINu");
  lcd.setCursor(0, 1);
  lcd.print("* - Anuluj/OK");

  noTone(BUZZER_PIN);
  for (int i = 0; i < 5; ++i) {
    tone(BUZZER_PIN, 4000, 80);
    delay(200);
  }

  long startTime = millis();
  while (millis() - startTime < 3000) {
    maskBtn.update();
    if (maskBtn.rose()) {
      tone(BUZZER_PIN, 600, 300);
      message("Anulowanie");
      clearPin();
      changeMaskingIcon(mask_before);
      masking = mask_before;
      initScreen();
      return;
    }
  }

  lcd.clear();
  lcd.blink();
  lcd.print("Podaj nowy PIN...");
  lcd.setCursor(0, 1);
  uint8_t bitCount;
  uint16_t state;
  uint16_t lastState;
  clearPin();

  do {
    do {
      lastState = state;
      state = readNumber();
      bitCount = countBits(state);
      maskBtn.update();
      if (maskBtn.fell()) {
        if (enteredCount < 1) {
          tone(BUZZER_PIN, 600, 300);
          message("Anulowanie");
          clearPin();
          initScreen();
          changeMaskingIcon(mask_before);
          masking = mask_before;
          return;
        }
        goto saveNewPin;
      }
    } while (bitCount == 0 || lastState == state);

    int8_t oldestBit = oldestBitPosition(state);

    char hexString[2];
    itoa(oldestBit, hexString, 16);
    enteredPin[enteredCount++] = hexString[0];

    printEnteredPin();
  } while (enteredCount < PIN_LEN);

  lcd.clear();

  saveNewPin:
    for (int i = 0; i < PIN_LEN; ++i) {
      if (i < enteredCount) {
        setPin[i] = enteredPin[i];
      } else {
        setPin[i] = 0;
      }
    }
  clearPin();
  changeMaskingIcon(mask_before);
  masking = mask_before;
  tone(BUZZER_PIN, 1200, 200);
  message("Zmieniono PIN!");
}

uint16_t readNumber() {
  uint16_t num = 0;
  for (int i = 2; i < 6; ++i) {
    digitalWrite(i, LOW);
    for (int j = 9; j < 13; ++j) {
      reading = digitalRead(j);
      num <<= 1;
      num += !reading;
    }
    for (int i = 2; i < 6; ++i) {
      digitalWrite(i, HIGH);
    }
  }
  return num;
}

uint8_t countBits(uint16_t num) {
  // Counting bits set, Brian Kernighan's way
  // from http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
  // count the number of bits set in num
  uint8_t c; // c accumulates the total bits set in v
  for (c = 0; num; c++) {
    num &= num - 1; // clear the least significant bit set
  }

  return c;
}

int8_t oldestBitPosition(uint16_t num) {
  for (int8_t pos = 15; pos >= 0; --pos) {
    if ((num >> pos) & 1) {
      return pos;
    }
  }
  return 0;
}

void wrongPin() {
  digitalWrite(LED_R_PIN, HIGH);
  lcd.noCursor();
  lcd.noBlink();
  lcd.clear();
  lcd.home();
  lcd.print("Zle haslo!");
  for (int i = 0; i < 3; i++) {
    tone(BUZZER_PIN, 1000, 100);
    delay(300);
  }

  digitalWrite(LED_R_PIN, LOW);
  clearPin();
  initScreen();
}

void goodPin() {
  maskBtn.update();
  digitalWrite(LED_G_PIN, HIGH);
  lcd.noCursor();
  lcd.noBlink();
  lcd.clear();
  lcd.home();
  lcd.print("Otwieranie!");
  lcd.setCursor(0, 1);
  lcd.print("* - zmien kod");
  tone(BUZZER_PIN, 2000, 1200);
  long startTime = millis();
  while (millis() - startTime < 3000) {
    maskBtn.update();
    if (maskBtn.rose()) {
      changePin();
      clearPin();
      initScreen();
      digitalWrite(LED_G_PIN, LOW);
      return;
    }
  }

  digitalWrite(LED_G_PIN, LOW);
  message("Zamykanie...");
  clearPin();
  initScreen();
}

void clearSecondLine() {
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(0, 1);
}

void printEnteredPin() {
  clearSecondLine();
  if (masking) {
    for (int i = 0; i < enteredCount; ++i) {
      lcd.print('*');
    }
  } else {
    lcd.print(enteredPin);
  }
}

void changeMaskingIcon(bool masking) {
  lcd.setCursor(15, 0);
  lcd.print(masking ? '*' : 'A');
  lcd.setCursor(enteredCount, 1);
}

void loop() {
  uint16_t number = 0;
  lastNumber = 0;

  do {
    maskBtn.update();
    if (maskBtn.fell()) {
      masking = !masking;
      changeMaskingIcon(masking);
      printEnteredPin();

      lastInterationTime = millis();
      lcd.backlight();
    }
    if (millis() - lastInterationTime > SCREEN_OFF_TIMEOUT) {
      clearPin();
      printEnteredPin();
      lcd.noBacklight();
    }
    lastNumber = number;
    number = readNumber();
  } while (number != 0 || lastNumber == number);

  lastInterationTime = millis();
  lcd.backlight();

  delay(20);
  uint8_t numBits = countBits(lastNumber);
  int8_t oldestBit = oldestBitPosition(lastNumber);

  char hexString[2];
  itoa(oldestBit, hexString, 16);
  enteredPin[enteredCount++] = hexString[0];

  printEnteredPin();

  bool pinGood = true;
  for (int pos = 0; pos < PIN_LEN; ++pos) {
    if (enteredPin[pos] != setPin[pos])
      pinGood = false;
  }

  if (pinGood) {
    goodPin();
    lastInterationTime = millis();
  }

  if (enteredCount == PIN_LEN) {
    wrongPin();
    return;
  }
}
```

### 7. Dokumentacja fizycznej realizacji projektu, prezentująca i potwierdzająca proces wytwórczy

### ![photo_1_2024-01-09_20-56-47](./assets/photo_1_2024-01-09_20-56-47.jpg)

### 8. Uruchamianie i testowanie zrealizowanego rozwiązania w kontekście prezentacji sposobu działania i praktycznego zastosowania urządzenia

| Opis                                               |                           Zdjęcie                            |
| -------------------------------------------------- | :----------------------------------------------------------: |
| Domyślny status - prośba o podanie hasła           | <img src="./assets/photo_2_2024-01-09_20-56-47.jpg" alt="photo_2_2024-01-09_20-56-47" style="zoom: 50%;" /> |
| Otwarcie po wpisaniu poprawnego hasła              | <img src="./assets/photo_7_2024-01-09_20-56-47.jpg" alt="photo_7_2024-01-09_20-56-47" style="zoom: 50%;" /> |
| Zmiana PINu<br />(dostępna po wpisaniu poprawnego) | <img src="./assets/photo_8_2024-01-09_20-56-47.jpg" alt="photo_8_2024-01-09_20-56-47" style="zoom:50%;" /><br /><img src="./assets/photo_3_2024-01-09_20-56-47.jpg" alt="photo_3_2024-01-09_20-56-47" style="zoom: 50%;" /> |
| Anulowanie zmiany PINu                             | <img src="./assets/photo_5_2024-01-09_20-56-47.jpg" alt="photo_5_2024-01-09_20-56-47" style="zoom: 50%;" /> |
| Dokończenie zmiany PINu                            | <img src="./assets/photo_6_2024-01-09_20-56-47.jpg" alt="photo_6_2024-01-09_20-56-47" style="zoom:50%;" /> |
| Podanie złego hasła                                | <img src="./assets/photo_2_2024-01-09_21-10-48.jpg" alt="photo_2_2024-01-09_21-10-48" style="zoom:50%;" /> |
| Tryb ukrytych znaków                               | <img src="./assets/photo_3_2024-01-09_21-10-48.jpg" alt="photo_3_2024-01-09_21-10-48" style="zoom:50%;" /> |
| Wygaszenie ekranu przy bezczynności                | <img src="./assets/photo_14_2024-01-09_20-56-47.jpg" alt="photo_14_2024-01-09_20-56-47" style="zoom:50%;" /> |

### 9. Krótka instrukcja optymalnej obsługi urządzenia i aplikacji

#### Uruchomienie i nadanie PINu

Uruchomienie następuje samoczynnie po podłączeniu zasilania. Zamek przejdzie w tryb zamknięty.

Aby zmienić PIN, należy otworzyć zamek pinem domyślnym, a następnie postępować zgodnie z instrukcjami z kroku [Zmiana PINu](#zmiana-pinu).

Domyślny PIN to `7355608`

#### Zmiana PINu

1. Otwórz zamek
2. W trakcie, gdy wyświetlany jest komunikat "* - zmien kod" naciśnij przycisk '*'
3. Zamek wyda 5 krótkich dźwięków i przejdzie w tryb zmiany kodu
4. Jeśli chcesz anulować zmianę kodu naciśnij '*' bez wpisywania innych znaków
5. Jeśli chcesz dokonać zmiany kodu – wpisz nowy kod na klawiaturze i zatwierdź przyciskiem '*'
   - Uwaga: Jeśli osiągnięta zostanie maksymalna długość kodu, zostanie on automatycznie zatwierdzony
6. Kod zostanie zmieniony, co będzie potwierdzone odpowiednim komunikatem i krótkim dźwiękiem

#### Zapomniane hasło

W przypadku zapomnienia hasła należy dokonać restartu urządzenia - zostanie przywrócony PIN domyślny.

### 10. Merytoryczne podsumowanie zalet i wad oraz możliwości dalszego rozwoju wspaniałego pomysłu technicznego

#### Zalety i wady

Zaletą takiego projektu jest możliwość zabezpieczenia niemal dowolnego zamka elektrycznego. Podawane przez kontroler napięcie może zostać użyte do sterowania zamkami, przekaźnikami, solenoidami, tranzystorami i wieloma innymi komponentami.

Możliwość zmiany kodu na dowolną kombinację zapewnia zwiększone bezpieczeństwo i wygodę. Kod można skompilować tak, aby przyjmował kody o długości nawet 14 znaków, co w połączeniu z 16 dostępnymi znakami daje $16^{14}=72057594037927936$ kombinacji (ponad 72 biliardy, $7.2 × 10^{16}$). Złamanie przez atak brute-force jest niewykonalne w dostępnym czasie.

Wadą w obecnej wersji jest brak pamięci kodu po utracie zasilania - można to naprawić poprzez dodanie pamięci EEPROM i zapis kodu w tej pamięci w zaszyfrowanej postaci.

#### Możliwości rozbudowy

Do projektu można dodać przekaźnik 250V w celu sterowania urządzeniami sieciowymi.

Można wykorzystać pamięć EEPROM lub flash w celu przechowania kodu pomiędzy uruchomieniami.

Można zaimplementować więcej niż jeden kod w celu obsługi wielu użytkowników z różnymi kodami.

